import axios from 'axios';
import axiosConfig from '../axiosConfig.js';

class PortfolioAPI{
	static loadPortfolio(token){
		return dispatch => {
			dispatch({
				type: 'MY_PORTFOLIO_FETCHING'
			})
			axios.get('/portfolios/myportfolio/',
				axiosConfig(token))
			.then(
				(response) => {
					console.log('Successfully Fetched Portfolio')
					dispatch({
						type: 'MY_PORTFOLIO_FETCH_SUCCESSFUL',
						coinholdings: response.data
					})
				}
			)
			.catch(
				(error) => {
					console.log('Portfolio Fetch Failed')
					dispatch({
						type: 'MY_PORTFOLIO_FETCH_FAILURE'
					})
				}
			)
		}
	}
}

export default PortfolioAPI