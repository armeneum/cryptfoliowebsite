const initState = require('./PortfolioInitState.json');

export default function portfolio(state = initState, action) {
	switch (action.type) {
		case "MY_PORTFOLIO_FETCHING":
			return	(
				{
					fetching: true,
					error: false
				}
			)
		case "MY_PORTFOLIO_FETCH_SUCCESSFUL":
			return	(
				{
					coinholdings: action.coinholdings,
					fetching: false,
					error: false
				}
			)
		case "MY_PORTFOLIO_FETCH_FAILURE":
			return (
				{
					fetching: false,
					error: true
				}
			)
		default:
			return state;
	}
}