import initState from './TransactionsInitState.json'

export default function transactions(state = initState, action){
	switch(action.type){
		case "MY_TRANSACTIONS_FETCHING":
			console.log("Fetching Transactions")
			return {
				fetching: true,
				error: false
			}
		case "MY_TRANSACTIONS_FETCH_SUCCESSFUL":
			console.log('Successfully Fetched Transactions')
			return {
				trades: action.trades,
				transfers: action.transfers,
				fetching: false,
				error: false
			}
		case "MY_TRANSACTIONS_FETCH_FAILURE":
			console.log('Failed to Fetch Transactions')
			return {
				fetching: false,
				error: true
			}
		default:
			return state
	}
}