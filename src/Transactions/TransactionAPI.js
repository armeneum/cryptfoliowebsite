import axios from 'axios'
import axiosConfig from '../axiosConfig'

export default class TransactionAPI{

	static addTrade(token, trade){
		console.log("SAVE TRADE")
		return dispatch => {
			dispatch({
				type: "TRADE_SAVING"
			})
			axios.post('/trades/', trade)
			.then(
				(trades) => {
					dispatch({
						type: "TRADE_SAVE_SUCCESSFUL",
						trades: trades.data
					})
				}
			)
			.catch(
				(error) => {
					dispatch({
						type: "TRADE_SAVE_FAILURE"
					})
				}
			)
		}
	}

	static addTransfer(token, transfer){
		console.log("SAVE TRANSFER")
		// return dispatch => {
		// 	dispatch({
		// 		type: "TRANSFER_SAVING"
		// 	})
		// 	axios.post('/trades/')
		// }
	}

	static getTransfersAxiosCall(token){
		return axios.get('/transfers/', axiosConfig(token))
	}

	static getTradesAxiosCall(token){
		return axios.get('/trades/', axiosConfig(token))
	}

	static getTransactions(token){
		return dispatch => {
			dispatch({
				type: "MY_TRANSACTIONS_FETCHING"
			})
			axios.all([TransactionAPI.getTradesAxiosCall(token),TransactionAPI.getTransfersAxiosCall(token)])
			.then(axios.spread(
				(trades, transfers) =>	{
					dispatch({
						type: "MY_TRANSACTIONS_FETCH_SUCCESSFUL",
						trades: trades.data,
						transfers: transfers.data
					})
				}
			))
			.catch(
				(error) => {
					dispatch({
						type: "MY_TRANSACTIONS_FETCH_FAILED"
					})
				}
			)
		}
	}
}