import {combineReducers} from 'redux'
import {routerReducer} from 'react-router-redux'
import userReducer from './User/userReducer'
import portfolioReducer from './Portfolio/PortfolioReducer'
import coinPriceReducer from './CoinPrices/CoinPricesReducer'
import transactionReducer from './Transactions/TransactionsReducer'

const rootReducer = combineReducers({
	routing: routerReducer,
	user: userReducer,
	portfolio: portfolioReducer,
	coinPrices: coinPriceReducer,
	transactions: transactionReducer
})

export default rootReducer;