const initState = require('./userInitState.json');

export default function user(state = initState, action) {
	switch (action.type) {
		case 'LOGIN_SUCCESSFUL':
			return Object.assign(
				{},
				state,
				{
					currentUser: {
						token: action.token,
					},
					error: false
				}
			)
		case 'LOGIN_FAILED':
			return Object.assign(
				{},
				state,
				{
					currentUser: {},
					error: true
				}
			)
		default:
			return state;
	}
}