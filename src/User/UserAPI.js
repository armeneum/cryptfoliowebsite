import axios from 'axios';
import axiosConfig from '../axiosConfig.js';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

class UserAPI{

	static logIn(email,password){
		return dispatch => {
			console.log("Trying to login user: " + email);
			console.log(axiosConfig())
			axios.post('/get_auth_token/', {
				username: email,
				password: password,
			},axiosConfig())
			.then(
				(response) => {
					console.log("Successfully Logged In User: " + email);
					dispatch({
						type: 'LOGIN_SUCCESSFUL',
						token: response.data.token
					})
					dispatch(push('/'))
				}
			)
			.catch(
				(error) => {
					debugger
					console.log("Failed to Login User: " + email);
					dispatch({
						type: 'LOGIN_FAILED'
					})
				}
			);
		}
	}	
}

function mapDispatchToProps(dispatch){
	return {}
}

function mapStateToProps(state){
	return {
		user: state.user.currentUser,
		error: state.user.error,
		history: state.history
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(UserAPI)