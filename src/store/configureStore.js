import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from '../rootReducer';
import thunk from 'redux-thunk';

export default function configureStore(routerMiddleware) {
  return createStore(
	rootReducer,
	compose(
		applyMiddleware(thunk, routerMiddleware),
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	)
  );
}
