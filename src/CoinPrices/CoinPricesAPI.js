import axios from 'axios';
import axiosConfig from '../axiosConfig.js';

class CoinPricesAPI{

	static loadMyCoinPrices(token){
		return dispatch => {
			dispatch({type: 'MY_COIN_PRICES_FETCHING'})
			axios.get('/coinprices/mycoinprices/',
				axiosConfig(token))
			.then(
				(response) => {
					console.log("Successfully Fetched Coin Prices");
					dispatch({
						type: 'MY_COIN_PRICES_FETCH_SUCCESSFUL',
						coinPrices: response.data
					})
				}
			)
			.catch(
				(error) => {
					console.log("Failed to Fetch Coin Prices");
					dispatch({
						type: 'MY_COIN_PRICES_FETCH_FAILURE',
					})
				}
			)
		}
	}

}

export default CoinPricesAPI