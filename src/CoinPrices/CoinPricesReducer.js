const initState = require('./CoinPricesInitState.json');

export default function coinPrices(state = initState, action) {
	switch (action.type) {
		case 'MY_COIN_PRICES_FETCHING':
			return {
				fetching: true,
				error: false
			}
		case 'MY_COIN_PRICES_FETCH_SUCCESSFUL':
			return (
				{
					prices: action.coinPrices,
					fetching: false,
					error: false
				}
			)
		case 'MY_COIN_PRICES_FETCH_FAILURE':
			return (
				{
					fetching: false,
					error: true
				}
			)
		default:
			return state;
	}
}