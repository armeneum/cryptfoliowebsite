import React, { Component } from 'react'
import App from './App.js'
import DashboardWidget from './DashboardWidget.js'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { push } from 'react-router-redux'

class Dashboard extends Component {

	componentDidMount(){
		if(this.props.user.token === undefined){
			this.props.redirectTo('/')
		}
	}

	render(){
		return (
			<App>
				<DashboardWidget />
			</App>
		)
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		redirectTo: (suffix) => push(suffix)
	}, dispatch)
}

function mapStateToProps(state){
	return {
		user: state.user.currentUser
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)