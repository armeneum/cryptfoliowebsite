import React, {Component} from 'react'
import highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'
import styled from 'styled-components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
//import PortfolioAPI from '../portfolio/portfolioApi.js'

highcharts.setOptions({
	lang: {
		thousandsSep: ','
	}
})

const ChartContainer = styled.div`
	padding: 30px;
`

const chartOptions = (data) => {
	return {
		// rangeSelector: {
		// 	selected: 4
		// },

		title: {
			text: ''
		},

		credits: {
			text: 'https://www.cryptfolio.net',
			href: 'http://localhost:3000'
		},

		series: [{
			data: data,
			tooltip: {
				//eslint-disable-next-line no-template-curly-in-string
				pointFormat: '<b>${point.y:,.2f}</b>',
				valueDecimals: 2
			}
		}]
	}
}

class Performance extends Component{
	chartData(){
		return	[
			[new Date(2010, 1, 10).getTime(), 4000.00],
			[new Date(2011, 3, 17).getTime(), 5000.00],
			[new Date(2012, 6, 3).getTime(), 5500.00],
			[new Date(2013, 2, 12).getTime(), 6000.00],
			[new Date(2014, 8, 30).getTime(), 7000.00],
			[new Date(2015, 7, 14).getTime(), 10000.00],
			[new Date(2016, 3, 2).getTime(), 11000.00],
			[new Date(2017, 2, 5).getTime(), 12000.00],
			[new Date(2018, 6, 26).getTime(), 20000.00],
		]
	}

	render(){
		return (
			<ChartContainer>
				<HighchartsReact
					highcharts={highcharts}
					constructorType={'stockChart'}
					options={chartOptions(this.chartData())}
				/>
			</ChartContainer>
		)
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({

	},dispatch)
}

function mapStateToProps(state){
	return {
		portfolio: state.portfolio
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(Performance)