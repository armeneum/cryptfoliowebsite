import React, { Component } from 'react'
import '../Stylesheets/SignUpForm.css'
import lock from '../Assets/lock.svg'
import mail from '../Assets/mail.svg'
import UserAPI from '../User/UserAPI.js'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class LoginForm extends Component {
	constructor(props){
		super(props);
		this.state = {email:"", password:""};
		this.changeEmail = this.changeEmail.bind(this);
		this.changePassword = this.changePassword.bind(this);
		this.checkLoginStatus = this.checkLoginStatus.bind(this);
	}

	changeEmail(event) {
		const email = event.target.value;
		this.setState({email: email, emailInputColor: "white", logInClickResponse: ""});
	}

	changePassword(event) {
		const password = event.target.value;
		this.setState({password: password, passwordInputColor: "white", logInClickResponse: ""})
	}

	checkLoginStatus(){
		const state = {
			'token': this.props.user.token,
			'error': this.props.error
		}
		if (state.token){
			return <div className="signup-form-message"><label className="signup-form-message-label" style={{color: "darkgreen"}}>Log In Successful!</label></div>
		}else if (state.error){
			return <div className="signup-form-message"><label className="signup-form-message-label" style={{color: "red"}}>Log In Failed</label></div>
		}else{
			return <div></div>
		}
	}

	render(){
		return (
			<div className="signup-form">				
				<h1 id="register-heading" className="signup-form-element"> Log In </h1>
				<span id="email-form" className="signup-form-element">
					<img id="mail-icon" className="icon" draggable="false" alt="Mail" src={mail}/>
					<input id="email-input" type="text" placeholder="Email" autoComplete="email" value={this.state.email} onChange={this.changeEmail} style={{backgroundColor: this.state.emailInputColor}}/>
				</span>
				<span id="password-form" className="signup-form-element password-form">
					<img className="lock-icon icon" draggable="false" alt="Lock" src={lock}/>
					<input id="password-input" type="password" placeholder="Password" autoComplete="new-password" value={this.state.password} onChange={this.changePassword} style={{backgroundColor: this.state.passwordInputColor}}/>
				</span>
				{this.checkLoginStatus()}
				<div id="signup-form-links" className="signup-form-element">
					<button id="signup-form-button" onClick={() => this.props.logIn(this.state.email,this.state.password)}>Log In</button>
					<a id="already-member-link" href="# ">Not a member?</a>
				</div>
			</div>
		);
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		logIn: UserAPI.logIn
	}, dispatch)
}

function mapStateToProps(state){
	return {
		user: state.user.currentUser,
		error: state.user.error
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginForm)