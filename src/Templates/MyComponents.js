// import { Component } from 'react'
import styled from 'styled-components'

export const Spacing = styled.div`
	display: inline-block;
	width: 10px;
`

export const RightAlign = styled.div`
	width: max-content;
	margin-left: auto;
	margin-right: 0;
`