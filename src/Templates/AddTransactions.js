import React, {Component} from 'react';

//User-defined Components
import TradeForm from './TradeForm.js';
import TransferForm from './TransferForm.js';

//Material UI
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

//Styling
import styled from 'styled-components';
import 'react-select/dist/react-select.css'
import 'react-virtualized-select/styles.css'

const formWidth = '90%'

const AddTransactionsContainer = styled.div`
	padding-top 15px;
`

const Header = styled.h2`
	width: max-content;
	margin: auto;
`

const CenterWithBottomPadding = styled.div`
	width: max-content;
	margin: 0 auto;
	padding-bottom: -15px;
`

const TradeOrTransferSwitch = styled(Switch)`

`

const TradeOrTransferComponent = styled.div`
	width: ${formWidth};
	margin: auto;
	padding-bottom: 15px;
	background-color: transparent;
`

class AddTransactions extends Component{

	constructor (props) {
		super(props)

		this.state = {
			onAddTradeForm: false,
		}

		this.handleSwitchChange = this.handleSwitchChange.bind(this)
		this.handleTextChange = this.handleTextChange.bind(this)
	}

	loadSelectedComponent = () => {
		if (this.state.onAddTradeForm){
			return <TradeForm/>
		}else{
			return <TransferForm/>
		}
	}

	handleTextChange = name => event => {
		if (event){
			this.setState({
				[name]: event.value,
			});
		}else{
			this.setState({
				[name]: null,
			});
		}
	}

	handleSwitchChange = name => event => {
		this.setState({
			[name]: event.target.checked
		})
	}

	tradeOrTransfer = (negate=false) => {
		if((this.state.onAddTradeForm && negate===false) ||
		(!this.state.onAddTradeForm && negate===true)){
			return "Trade"
		}else{
			return "Transfer"
		}
	}

	getSwitchLabel = () => {
		let trade;
		let transfer;
		const selectedStyle = {
			fontWeight: 'bold',
			width: 'max-content',
			color: 'black'
		}
		if(this.state.onAddTradeForm){
			trade = <div style={selectedStyle}>Trade</div>
			transfer = <div style={{height: 'max-content'}}>Transfer</div>
		}else{
			trade = <div>Trade</div>
			transfer = <div style={selectedStyle}>Transfer</div>
		}
		return <div>{trade}{transfer}</div>
	}

	render(){

		return (			
			<AddTransactionsContainer>
				<Header>
					Add Transaction
				</Header>
				<CenterWithBottomPadding>
					<FormControlLabel
						control={
							<TradeOrTransferSwitch
								checked={this.state.onAddTradeForm}
								onChange={this.handleSwitchChange('onAddTradeForm')}
								color="primary"
							/>
						}
						label= { this.getSwitchLabel() }
					/>
				</CenterWithBottomPadding>
				<TradeOrTransferComponent>
					{this.loadSelectedComponent()}
				</TradeOrTransferComponent>
			</AddTransactionsContainer>
		)
	}
}

export default AddTransactions