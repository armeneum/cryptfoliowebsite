import React, { Component } from 'react'
import '../Stylesheets/SignUpForm.css'
import lock from '../Assets/lock.svg'
import mail from '../Assets/mail.svg'

class SignupForm extends Component {
	constructor(props){
		super(props);
		this.state = {
			email:"",
			password:"",
			confirmPassword:"",
			emailWarnings:[],
			passwordWarnings:[],
			passwordInputColor:"white",
			warnings:""
		};
		this.validationOnSignUpClick = this.validationOnSignUpClick.bind(this);
		this.registerUser = this.registerUser.bind(this);
		this.changeEmail = this.changeEmail.bind(this);
		this.changePassword = this.changePassword.bind(this);
		this.changeConfirmPassword = this.changeConfirmPassword.bind(this);
	}

	registerUser(email, password){
		console.log("Trying to create user: " + email);
		console.log(this.props.getFromState('axios'))
		this.props.getFromState('axios').post('/users/', {
			username: email,
			password: password,
		})
		.then(
			(response) => {
				if (response.status === 201){
					console.log("Successfully Created User: " + this.state.email);
					this.setState({signUpClickResponse: <div className="signup-form-message"><label className="signup-form-message-label" style={{color: "darkgreen"}}>Registration Successful!</label></div>})
				}else{
					this.setState({signUpClickResponse: <div className="signup-form-message"><label className="signup-form-message-label" style={{color: "darkgreen"}}>wtf just happened?</label></div>})
				}
			},
			(error) => {
				console.log("Failed to Create User: " + email);
				console.log(error.response);
				if (error.response !== undefined){
					const emailErrors = error.response.data.email;
					const passwordErrors = error.response.data.password;
					var message;
					if (emailErrors !== undefined && emailErrors.length > 0){
						const emailError = emailErrors[0];
						if (emailError.includes("exists")){
							message = "Email Already Registered";
						}else if (emailError === "Enter a valid email address."){
							message = "Invalid Email (must contain @ and .)";
						}else{
							message = "Registration Failed";
						}
					}else if (passwordErrors !== undefined && passwordErrors.length > 0){
						//const passwordError = passwordErrors[0];
						message = "Invalid Password";
					}else{
						message = "Unknown Error"
					}
				}else{
					message = "Server Request Failure"
				}
				this.setState({signUpClickResponse: <div className="signup-form-message"><label className="signup-form-message-label" style={{color: "red"}}>{message}</label></div>})
			}
		)
	}

	emailIsValid(email) {
		return email.match("^.+@.+\\..+$");
	}

	passwordIsValid(password) {
		return (password.match("^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[_!#$%&? \"])[a-zA-Z0-9_!#$%&? ]+$"))
	}

	validationOnSignUpClick(event) {
		var password = this.state.password;
		var email = this.state.email;
		// var passwordWarnings = this.state.passwordWarnings;
		// var emailWarnings = this.state.emailWarnings;

		// // vvv FOR TESTING ONLY DELETE vvv
		// if (email.length === 0){
		// 	email = "User" + Math.floor(Math.random()*10000) + "@gmail.com"
		// 	password = "Password_123";
		// 	this.setState({email: email, password: password, confirmPassword: password});
		// 	passwordWarnings = "";
		// }// ^^^ FOR TESTING ONLY DELETE ^^^

		const emailIsValid = this.emailIsValid(email);
		const passwordIsValid = this.passwordIsValid(password);


		var stateUpdate = {'emailWarnings': "", 'passwordWarnings': ""};
		stateUpdate["password"] = "";
		stateUpdate["confirmPassword"] = "";
		if (emailIsValid && passwordIsValid){
			stateUpdate["email"] = "";
			console.log("Sending POST Request for User: " + this.state.email)
			this.registerUser(email,password);
		}else{
			stateUpdate["signUpClickResponse"] = "";
			if (!emailIsValid){
				stateUpdate['emailInputColor'] = "lightpink";
				if(email.length === 0){
					stateUpdate['emailWarnings'] = [<li key="email-blank" className="password-warning"> Email cannot be blank. </li>]
				}
			}
			if (!passwordIsValid){
				stateUpdate['passwordInputColor'] = "lightpink";
				if(password.length === 0){
					stateUpdate['passwordWarnings'] = [<li key="password-blank" className="password-warning"> Password cannot be blank. </li>]
				}
			}
		}
		stateUpdate['warnings'] = this.combineWarnings(stateUpdate["emailWarnings"],stateUpdate["passwordWarnings"])
		this.setState(stateUpdate);
	}

	resetPasswordInputColor(passwordWarningsLength) {
		if(passwordWarningsLength === 0){
			this.setState({passwordInputColor: "white"})
		}
	}

	resetEmailInputColor(emailWarnings) {
		if(emailWarnings === []){
			this.setState({emailInputColor: "white"})
		}
	}

	changeEmail(event) {
		const email = event.target.value;
		const emailWarnings = this.emailValidationWarning(email);
		const warnings = this.combineWarnings(emailWarnings,this.state.passwordWarnings);
		this.setState({email: email, emailWarnings: emailWarnings, emailInputColor: "white", warnings: warnings, signUpClickResponse: ""});
	}

	changePassword(event) {
		const password = event.target.value;
		const passwordWarnings = this.passwordValidationWarnings(password,this.state.confirmPassword);
		const warnings = this.combineWarnings(this.state.emailWarnings,passwordWarnings);
		this.setState({password: password, passwordWarnings: passwordWarnings, passwordInputColor: "white", warnings: warnings, signUpClickResponse: ""})
	}

	changeConfirmPassword(event) {
		const confirmPassword = event.target.value;
		const passwordWarnings = this.passwordValidationWarnings(this.state.password,confirmPassword);
		const warnings = this.combineWarnings(this.state.emailWarnings,passwordWarnings);
		this.setState({confirmPassword: confirmPassword, passwordWarnings: passwordWarnings, passwordInputColor: "white", warnings: warnings, signUpClickResponse: ""});
	}

	emailValidationWarning(email){
		if (email.length > 0 && !this.emailIsValid(email)){
			return [<li key="email-validation-warning" className="password-warning">Invalid Email (example@for.you)</li>]
		}else{
			return [];
		}
	}

	combineWarnings(emailWarnings,passwordWarnings){
		const combinedWarnings = emailWarnings.concat(passwordWarnings)
		if (combinedWarnings.length === 0){
			return "";
		}else{
			return <ul className="password-warning-list">{combinedWarnings}</ul>
		}
	}

	passwordValidationWarnings(password, confirmPassword) {
		var lines = [];
		var regexPatternToWarningMessage = {
			"(.{8,})": "Password must contain at least 8 characters.",
			"([a-z])": "Password must contain at least one lowercase letter.",
			"([A-Z])": "Password must contain at least one capital letter.",
			"([0-9])": "Password must contain at least one digit [0-9].",
			"([_!#$%&? \"])": "Password must contain at least one special character [_!#$%&?\"] or space.",
			"(^[a-zA-Z0-9_!#$%&? ]*$)": "Password must not contain invalid characters."
		}

		const pass = password;
		const confPass = confirmPassword;
		if (!(pass.length === 0) || !(confPass.length === 0)) {
			for (var regexPattern in regexPatternToWarningMessage){
				if (!pass.match(regexPattern)) {
					lines.push(<li key={regexPattern} className="password-warning">{regexPatternToWarningMessage[regexPattern]}</li>);
				}
			}
			if (!(pass === confPass)){
				lines.push(<li key="passwords-unmatched" className="password-warning"> Passwords must match. </li>)
			}
		}
		return lines;
	}

	render(){
		return (
			<div className="signup-form">
				<h1 id="register-heading" className="signup-form-element"> Register </h1>
				<span id="email-form" className="signup-form-element">
					<img id="mail-icon" className="icon" draggable="false" alt="Mail" src={mail}/>
					<input id="email-input" type="text" placeholder="Email" autoComplete="email" value={this.state.email} onChange={this.changeEmail} style={{backgroundColor: this.state.emailInputColor}}/>
				</span>
				<span id="password-form" className="signup-form-element password-form">
					<img className="lock-icon icon" draggable="false" alt="Lock" src={lock}/>
					<input id="password-input" type="password" placeholder="Password" autoComplete="new-password" value={this.state.password} onChange={this.changePassword} style={{backgroundColor: this.state.passwordInputColor}}/>
				</span>
				<span id="confirm-password-form" className="signup-form-element password-form">
					<img className="lock-icon icon" draggable="false" alt="Lock" src={lock}/>
					<input id="confirm-password-input" type="password" placeholder="Confirm Password" autoComplete="new-password" value={this.state.confirmPassword} onChange={this.changeConfirmPassword} style={{backgroundColor: this.state.passwordInputColor}}/>
				</span>
				{this.state.warnings}
				{this.state.signUpClickResponse}
				<div id="signup-form-links" className="signup-form-element">
					<button id="signup-form-button" onClick={this.validationOnSignUpClick}>Sign Up</button>
					<a id="already-member-link" href="# ">Already a member?</a>
				</div>
			</div>
		);
	}
}

export default SignupForm
