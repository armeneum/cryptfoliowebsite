import React, {Component} from 'react';

//Styling

	//Libraries
// import styled from 'styled-components';
import { withTheme } from '@material-ui/core/styles';

	//My Styles
import { StyledTransactionForm, FormElement, StyledDateField, 
	StyledTimeField, StyledQuantityField, 
	CoinSelectContainer, BoughtSoldContainer,
	ExchangeSelect, StyledTransferTypeSelect,
	CoinSelect, ClearAndSaveButtons, FormElementLabel, StyledWalletAddressField
	} from './TransactionFormStyles';

//API
import TransactionAPI from '../Transactions/TransactionAPI';

class TransferForm extends Component{

	constructor(props){
		super(props)

		let today = new Date();
		let todayDate = today.toISOString().split("T")[0];
		let todayTime = today.toISOString().split("T")[1].substring(0,5);

		this.state = {
			date: todayDate,
			time: todayTime,
			transferType: "",
			exchange: 'N/A',
			quantity: null,
			fromAddress: null,
			toAddress: null,
			coin: null,
			price: null,
			coinTicker: ""
		}

		this.handleTextChange = this.handleTextChange.bind(this)
		this.handleDropdownChange = this.handleDropdownChange.bind(this)
		this.handleSelectChange = this.handleSelectChange.bind(this)
		this.clearForm = this.clearForm.bind(this)
		this.saveTransfer = this.saveTransfer.bind(this)
	}

	handleTextChange = name => event => {
		if (event){
			this.setState({
				[name]: event.value,
			});
		}else{
			this.setState({
				[name]: null,
			});
		}
	}

	handleDropdownChange = name => event => {
		this.setState({
			[name]: event.target.value,
		});
	}

	handleSelectChange = name => value => {
		this.setState({
			[name]: value
		})
	}

	clearForm(){
		this.setState({
			date: "",
			time: "",
			transferType: "",
			exchange: "",
			quantity: null,
			fromAddress: null,
			toAddress: null,
			coin: null,
			price: null
		})
	}

	saveTransfer(){
		TransactionAPI.addTransfer()
	}

	getBoughtSoldLabel(){
		switch(this.state.transferType){
			case "D":
				return "Received"
			case "S":
				return "Received Payment"
			case "W":
				return "Sent"
			case "P":
				return "Spent"
			default:
				return "Received/Sent"
		}
	}

	getBoughtSoldColor(){
		if (["D","S"].includes(this.state.transferType)){
			return	'rgb(0, 255, 0, 0.05)'
		}else if (["W","P"].includes(this.state.transferType)){
			return	'rgb(255, 0, 0, 0.05)'
		}else{
			return 'rgb(255, 255, 0, 0.05)'
		}
	}

	render(){
		return (
			<StyledTransactionForm>
				<FormElement>
					<StyledDateField
						value={ this.state.date }
						onChange={ this.handleTextChange('date') }
					/>
					<StyledTimeField
						value={ this.state.time }
						onChange={ this.handleTextChange('time') }
					/>
				</FormElement>
				<FormElement>
					<StyledTransferTypeSelect
						value={ this.state.transferType }
						onChange={ this.handleDropdownChange('transferType') }
					/>
				</FormElement>
				<FormElement>
					<ExchangeSelect
						value={ this.state.exchange }
						onChange={ this.handleSelectChange('exchange') }
					/>
				</FormElement>
				<FormElement>
				<FormElementLabel>{this.getBoughtSoldLabel()}</FormElementLabel>
					<BoughtSoldContainer
						borderColor={this.props.theme.palette.primary[100]}
						backgroundColor={this.getBoughtSoldColor()}
					>
						<StyledQuantityField
							value={ this.state.buyQuantity }
							onChange={ this.handleTextChange('buyQuantity') }
							suffix={ " " + this.state.coinTicker}
						/>
						<CoinSelectContainer>
							<CoinSelect
								value={this.state.coin}
								onChange={ this.handleSelectChange('coin') }
								label='Coin'
								placeholder=''
							/>
						</CoinSelectContainer>
					</BoughtSoldContainer>
				</FormElement>
				<FormElement>
					<StyledWalletAddressField
						label="From Address"
						style={{
							marginRight: "5%"
						}}
					/>
					<StyledWalletAddressField
						label="To Address"
						style={{
							marginLeft: "5%"
						}}
					/>
				</FormElement>
				<FormElement>
					<ClearAndSaveButtons
						clearFunc={ this.clearForm }
						saveFunc={ this.saveTransfer }
					/>
				</FormElement>
			</StyledTransactionForm>
		)
	}

}

export default withTheme()(TransferForm)