import React, { Component } from 'react'

//My Components
import { RightAlign, Spacing } from './MyComponents'

//Material UI
	//Components
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IntegrationReactSelect from './IntegrationReactSelect.js'
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
	//Icons
import Clear from '@material-ui/icons/Clear';
import Save from '@material-ui/icons/Save';

//Other Third Party Components
import NumberFormat from 'react-number-format';

//Styling
import styled from 'styled-components'


export const StyledTransactionForm = styled.div`
`

export const FormElementLabel = styled.h3`
	font-weight: normal;
	width: max-content;
	margin: auto;
`

export const FormElement = styled.div`
	padding: 15px;
`

export const BoughtSoldContainer = styled.div`
	padding: 15px;
	margin-top: 15px;
	border: 1px solid ${props => props.borderColor};
	border-radius: 10px;
	background-color: ${props => props.backgroundColor};
`

export const StyledTextField = styled(TextField).attrs({
	InputLabelProps: {
		shrink: true
	}
})`
	display: inline-block;
`

export const StyledDateField = styled(StyledTextField).attrs({
	id: "date",
	label: "Transaction Date",
	type: "date",
})`
	width: 45%;
	&&{margin-right: 5%;}
`

export const StyledTimeField = styled(StyledTextField).attrs({
	id: "time",
	label: "Transaction Time",
	type: "time",
})`
	width: 45%;
	&&{margin-left: 5%;}
`

export const StyledWalletAddressField = styled(StyledTextField).attrs({
	id: "walletAddress",
	label: "Address",
	type: "text",
})`
	width: 45%;
`

export class StyledQuantityField extends Component{
	render(){
		let suffix = this.props.suffix
		let onChange = this.props.onChange
		return (
			<StyledTextFieldForQualityField
				label="Quantity"
				id="formatted-numberformat-input"
				onChange= { onChange }
				InputProps={
					{
						inputComponent:NumberFormatCustom,
						inputProps: {
							suffix: suffix
						}
					}
				}
			/>
		)
	}
}

export const StyledTextFieldForQualityField = styled(StyledTextField)`
	width: 45%;
	&&{margin-right: 5%;}
`

export const StyledPriceField = styled(StyledTextField).attrs({
	label: "Price",
	id: "formatted-numberformat-input",
	InputProps: {
		inputComponent: NumberFormatCurrency
	}
})`
	width: 30%;
	&&{margin-left: 5%;}
`

export const CoinSelectContainer = styled.div`
	display: inline-block;
	width: 45%;
`

export class SaveButton extends Component{
	render(){
		return (
			<Button
				variant="raised" //flat, outlined, raised, fab
				color="primary"
				onClick={this.props.saveFunc}
			>
				Save
				<Save style={{paddingBottom: '1px', paddingLeft: '3px'}}/>
			</Button>
		)
	}
}

export class ClearButton extends Component{
	render(){
		return (
			<Button
				variant="raised" //flat, outlined, raised, fab
				color="secondary"
				onClick={this.props.clearFunc}
			>
				Clear
				<Clear style={{paddingBottom: '1px', paddingLeft: '1px'}}/>
			</Button>
		)
	}
}

export class ClearAndSaveButtons extends Component{
	render(){
		return(
			<RightAlign style={{paddingTop: '10px'}}>
				<ClearButton
					clearFunc={ this.props.clearFunc }
				/>
				<Spacing/>
				<SaveButton
					saveFunc={ this.props.saveFunc }
				/>
			</RightAlign>
		)
	}
}

function NumberFormatCustom(props){
	const { inputRef, onChange, ...other } = props;

	return (
		<NumberFormat
			{...other}
			ref={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			thousandSeparator
		/>
	);
}

function NumberFormatCurrency(props){
	const { inputRef, onChange, ...other } = props;

	return (
		<NumberFormat
			{...other}
			ref={inputRef}
			onValueChange={values => {
				onChange({
					target: {
						value: values.value,
					},
				});
			}}
			thousandSeparator
			prefix="$"
		/>
	);
}

const exchangeOptions = [
	{ label: 'N/A' },
	{ label: 'BitMEX' },
	{ label: 'Binance' },
	{ label: 'OKEx' },
	{ label: 'Huobi' },
	{ label: 'Bitfinex' },
	{ label: 'Upbit' },
	{ label: 'Bithumb' },
	{ label: 'HitBTC' },
	{ label: 'Bibox' },
	{ label: 'LBank' },
	{ label: 'GDAX' },
	{ label: 'Kraken' },
	{ label: 'Bit-Z' },
	{ label: 'BCEX' },
	{ label: 'Bitstamp' },
	{ label: 'Gate.io' },
	{ label: 'Bittrex' },
	{ label: 'IDAX' },
	{ label: 'Poloniex' },
	{ label: 'Gemini' },
	{ label: 'YoBit' },
	{ label: 'Kucoin' },
	{ label: 'Liqui' },
	{ label: 'Cryptopian' },
	{ label: 'IDEX' },
	{ label: 'CoinExchange' },
	{ label: 'COSS' },
	{ label: 'Cobinhood' }
].map(suggestion => ({
	value: suggestion.label,
	label: suggestion.label
}))

const coinOptions = [
	{label: 'Bitcoin'},
	{label: 'Ethereum'},	
].map(suggestion => ({
	value: suggestion.label,
	label: suggestion.label
}))

export const ExchangeSelect = styled(IntegrationReactSelect).attrs({
	label: 'Exchange',
	placeholder: '',
	defaultValue: 'N/A',
	suggestions: exchangeOptions
})`
`

export const CoinSelect = styled(IntegrationReactSelect).attrs({
	suggestions: coinOptions
})`
`

class TransferTypeSelect extends Component{
	render(){
		return (
			<FormControl
				fullWidth
			>
				<InputLabel htmlFor="transferTypeSelect" shrink={true}>Transfer Type</InputLabel>
				<Select
					value={ this.props.value }
					onChange={ this.props.onChange }
					inputProps={{
						name: 'transferType',
						id: 'transferTypeSelect'
					}}
					placeholder="Transfer Type"
				>
					<MenuItem value={"D"}>Deposit</MenuItem>
					<MenuItem value={"W"}>Withdrawal</MenuItem>
					<MenuItem value={"P"}>Purchase</MenuItem>
					<MenuItem value={"S"}>Sale</MenuItem>
				</Select>
			</FormControl>
		)
	}
}

//Styles
export const StyledTransferTypeSelect = styled(TransferTypeSelect)`
	width: 100%
`