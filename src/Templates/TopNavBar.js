import React, { Component } from 'react';
import '../Stylesheets/TopNavBar.css';
import styled from 'styled-components';
import graph from '../Assets/rising-graph.svg';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';

import { withTheme } from '@material-ui/core/styles';

const TopNavBarContainer = styled.div`
	background-color: ${props => props.backgroundColor};
	/*box-shadow: 0px 5px 5px grey;*/
	overflow: hidden;
	font-size: 17px;
	height: 60px;
	margin-right: 0;
	
	&::-webkit-scrollbar {
		background: transparent;
		width: 0;
	}
`

const Button = styled.button`
	float: right;
	height: 40px;
	margin-top: 10px;
	margin-right: 20px;
	padding: 9.5px 20px;
	border-radius: 12px;
	font-size: 14px;
	box-shadow: 0px 0px 1px 1px grey;
	background-color: ${props => props.color} ;
	color: #2F2F2F;
	border: 0px solid #2F2F2F;

	:hover {
		background-color: ${props => props.hoverColor};
		cursor: pointer;
	}

	:active {
		/*background-color: rgba(0,255,0,0.2);*/
		background-color: ${props => props.clickColor};
		cursor: pointer;
	}
`

class TopNavBar extends Component {
	constructor(props){
		super(props)
		this.state = {}
	}

	getButton(){
		const loggedIn = this.props.user.token !== undefined;
		let buttonText;
		let buttonLink;
		if (loggedIn){
			buttonLink = '/logout'
		}else if(this.props.page === '/signup'){
			buttonLink = '/login'
		}else{
			buttonLink = '/signup'
		}
		buttonText = buttonLink.substring(1,2).toUpperCase() + buttonLink.substring(2)
		return (
			<Button 
				color={this.props.theme.palette.primary[100]} 
				hoverColor={this.props.theme.palette.primary[200]} 
				clickColor={this.props.theme.palette.primary[300]}
				onClick={this.buttonClickHandler}
			>
			 	{buttonText}
			</Button>
		)
	}

	render(){
		return (
			<TopNavBarContainer backgroundColor={this.props.theme.palette.primary.main}>
				<a href="# ">
					<span id="logo-with-text" className="topnavbar-item">
						<img id="logo" src={graph} alt="Logo"/>
						<div id="cryptfolio-text">cryptfolio</div>
					</span>
				</a>
				{this.getButton()}
			</TopNavBarContainer>
		);
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		redirectTo: (path) => push(path)
	}, dispatch)
}

function mapStateToProps(state){
	return {
		user: state.user.currentUser,
		page: state.routing.location.pathname
	}
}

export default withTheme()(connect(mapStateToProps, mapDispatchToProps)(TopNavBar))