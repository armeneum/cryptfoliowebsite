import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { bindActionCreators } from 'redux';

class IndexRedirector extends Component {

	componentDidMount(){
		switch(this.props.user.token){
			case undefined:
				//this.props.history.push('/login')
				this.props.redirectTo('/login')
				break;
			default:
				this.props.redirectTo('/dashboard')
				break;
		}
	}

	render() {
		return (<div></div>);
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		redirectTo: (suffix) => push(suffix)
	}, dispatch)
}

function mapStateToProps(state){
	return {
		user: state.user.currentUser,
		error: state.user.error
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(IndexRedirector)