import React, { Component } from 'react'
import TopNavBar from './TopNavBar'

import red from '@material-ui/core/colors/red'
import blue from '@material-ui/core/colors/blue'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

import styled from 'styled-components'

const theme = createMuiTheme({
	palette: {
		primary: blue,
		secondary: red,
	}
});

const StyledAppContainer = styled.div`
	font-family: Courier New;
	position: fixed;
	top: 0;
	bottom: 0;
	right: 0;
	left: 0;
	background-color: ${props => props.backgroundColor}
	/*background-image: linear-gradient(45deg, #d7e0fb 0%, #93aeff 100%);*/
	overflow-y: scroll;
	overflow-x: hidden;
`

class App extends Component {
	render() {
		return (
			<MuiThemeProvider theme={theme}>
				<StyledAppContainer backgroundColor={theme.palette.primary[100]}>
					<TopNavBar/>
					{this.props.children}
				</StyledAppContainer>
			</MuiThemeProvider>
		);
	}
}

export default App