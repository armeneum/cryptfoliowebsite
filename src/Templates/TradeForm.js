import React, {Component} from 'react';

//Styling
	//Libraries
import { withTheme } from '@material-ui/core/styles';
	//My Styles
import { StyledTransactionForm, FormElement, StyledDateField, 
	StyledTimeField, StyledQuantityField, 
	CoinSelectContainer, FormElementLabel, BoughtSoldContainer,
	ExchangeSelect, CoinSelect, ClearAndSaveButtons
	} from './TransactionFormStyles';

//API
import TransactionAPI from '../Transactions/TransactionAPI';

import { connect } from 'react-redux'

class TradeForm extends Component{

	constructor (props) {
		super(props)

		let today = new Date();
		let todayDate = today.toISOString().split("T")[0];
		let todayTime = today.toISOString().split("T")[1].substring(0,5);

		this.state = {
			date: todayDate,
			time: todayTime,
			exchange: null,
			buyCoin: null,
			buyQuantity: null,
			sellCoin: null,
			sellQuantity: null,
		}

		this.handleSwitchChange = this.handleSwitchChange.bind(this)
		this.handleTextChange = this.handleTextChange.bind(this)
		this.handleQuantityTextChange = this.handleQuantityTextChange.bind(this)
		this.handleSelectChange = this.handleSelectChange.bind(this)
		this.clearForm = this.clearForm.bind(this)
		this.saveTrade = this.saveTrade.bind(this)
	}

	handleTextChange = name => event => {
		if (event){
			this.setState({
				[name]: event.value,
			});
		}else{
			this.setState({
				[name]: null,
			});
		}
	}

	handleQuantityTextChange = name => event => {
		if (event){
			this.setState({
				[name]: event.target.value,
			});
		}else{
			this.setState({
				[name]: null,
			});
		}
	}

	handleSwitchChange = name => event => {
		this.setState({
			[name]: event.target.checked
		})
	}

	handleSelectChange = name => value => {
		this.setState({
			[name]: value
		})
	}

	clearForm(){
		console.log("CLEAR")
		this.setState({
			date: "",
			time: null,
			exchange: null,
			buyCoin: null,
			buyQuantity: null,
			sellCoin: null,
			sellQuantity: null,
		})
	}

	saveTrade(){
		let fieldsContainNull = [
			this.state.date,
			this.state.exchange,
		   	this.state.buyCoin,
		   	this.state.buyQuantity,
		   	this.state.sellCoin,
		   	this.state.sellQuantity
		   	].reduce(
			(anyNull, currentField) =>
				anyNull || !currentField,
				false
		)
		if (fieldsContainNull){
			console.log("INVALID TRADE")
		}else{
			let trade = {
				date: this.state.date,
				time: this.state.time,
				exchange: this.state.exchange,
				buyCoin: this.state.buyCoin,
				buyQuantity: this.state.buyQuantity,
				sellCoin: this.state.sellCoin,
				sellQuantity: this.state.sellQuantity,
			}
			console.log('SAVED TRADE')
			//TransactionAPI.addTrade(this.props.token, trade)

		}
	}

	render(){

		return (
			<StyledTransactionForm>
				<FormElement>
					<StyledDateField
						value={ this.state.date }
						onChange={ this.handleTextChange('date') }
					/>
					<StyledTimeField
						value={ this.state.time }
						onChange={ this.handleTextChange('time') }
					/>
				</FormElement>
				<FormElement>
					<ExchangeSelect
						value={this.state.exchange}
						onChange={ this.handleSelectChange('exchange') }
					/>
				</FormElement>
				<FormElement>
				<FormElementLabel>Bought</FormElementLabel>
					<BoughtSoldContainer
						borderColor={this.props.theme.palette.primary[100]}
						backgroundColor='rgb(0, 255, 0, 0.05)'
					>
						<StyledQuantityField
							value={ this.state.buyQuantity }
							onChange={ this.handleQuantityTextChange('buyQuantity') }
						/>
						<CoinSelectContainer>
							<CoinSelect
								value={ this.state.buyCoin }
								onChange={ this.handleSelectChange('buyCoin') }
								label='Coin'
								placeholder=''
							/>
						</CoinSelectContainer>
					</BoughtSoldContainer>
				</FormElement>
				<FormElement>
				<FormElementLabel>Sold</FormElementLabel>
					<BoughtSoldContainer 
						borderColor={this.props.theme.palette.primary[100]}
						backgroundColor='rgb(255, 0, 0, 0.05)'
					>
						<StyledQuantityField
							value={ this.state.sellQuantity }
							onChange={ this.handleQuantityTextChange('sellQuantity') }
						/>
						<CoinSelectContainer>
							<CoinSelect
								value={ this.state.sellCoin }
								onChange={ this.handleSelectChange('sellCoin') }
								placeholder=''
								label='Coin'
							/>
						</CoinSelectContainer>
					</BoughtSoldContainer>
				</FormElement>
				<FormElement>
					<ClearAndSaveButtons
						clearFunc={ this.clearForm }
						saveFunc={ this.saveTrade }
					/>
				</FormElement>
			</StyledTransactionForm>
		)
	}
}

function mapStateToProps(state){
	return ({
		token: state.token
	})
}

export default withTheme()(connect(mapStateToProps)(TradeForm))