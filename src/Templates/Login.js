import React, { Component } from 'react';
import App from './App.js'
import LoginForm from './LoginForm'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { push } from 'react-router-redux'

class Login extends Component{

	componentDidMount(){
		if(this.props.user.token !== undefined){
			this.props.redirectTo('/dashboard')
		}
	}

	render(){
		return (
			<App>
				<LoginForm />
			</App>
		)
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		redirectTo: (suffix) => push(suffix)
	}, dispatch)
}

function mapStateToProps(state){
	return {
		user: state.user.currentUser
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)