import React, {Component} from 'react'
import highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import CoinPricesAPI from '../CoinPrices/CoinPricesAPI.js'
import PortfolioAPI from '../Portfolio/PortfolioAPI.js'

const ChartContainer = styled.div`
	padding: 30px 0px;
`

highcharts.setOptions({
	lang: {
		thousandsSep: ','
	}
})

// eslint-disable-next-line
const gradientColor = highcharts.map(
	highcharts.getOptions().colors,
	(color) => {
		return {
			radialGradient: {
				cx: 0.5,
				cy: 0.3,
				r: 0.7
			},
			stops: [
				[0, color],
				[1,highcharts.Color(color).brighten(-0.3).get('rgb')]
			]
		}
	}
)

const chartOptions = props => {
	return {
		colors: highcharts.getOptions().colors,
		chart: {
			plotBackgroundColor: null,
			plotBorderWidth: null,
			plotShadow: false,
			type: 'pie'
		},
		credits: {
			text: 'https://www.cryptfolio.net',
			href: 'https://www.cryptfolio.net'
		},
		plotOptions: {
			pie: {
				allowPointSelect: true,
				cursor: 'pointer',
				dataLabels: {
					enabled: true,
					format: '<b>{point.name}</b>: {point.percentage:.1f} %',
					style: {
						color: (highcharts.theme && highcharts.theme.contrastTextColor) || 'black'
					},
					connectorColor: 'silver'
				}
			}
		},
		tooltip: {
			formatter() {
				return '<b>' + props.quantities[this.key] + ' ' + props.coinNameToTicker[this.key] + '</b>'
			}
		},
		title: {
			// eslint-disable-next-line no-template-curly-in-string
			text: `<b>$${props.total.toFixed(2).replace(/\B(?=(\d{3})+\.)/g, ",")}</b>`
		},
		series: [{
			name: "",
			data: props.data
		}]
	}
}

class Portfolio extends Component{

	constructor(props){
		super(props)
		this.state = {}

		//this.highchartsOrError = this.highchartsOrError.bind(this)
	}

	componentWillMount(){
		Promise.all(
			[
				this.props.loadPortfolio(this.props.token),
				this.props.loadPortfolioPrices(this.props.token)
			]
		)
	}

	getPortfolioProps(){
		let portfolio = []
		let total = 0;
		let quantities = {}
		let coinNameToTicker = {}
		let coinholdings = this.props.portfolio.coinholdings
		let coinPrices = this.props.coinPrices.prices
		if (coinholdings !== undefined){
			for(let coinName in coinholdings){
				let quantity = coinholdings[coinName].quantity
				let lastTradePrice = coinPrices[coinName][0].price
				let value = quantity * lastTradePrice
				quantities[coinName] = parseFloat(quantity).toFixed(2).replace(/\B(?=(\d{3})+\.)/g, ",")
				coinNameToTicker[coinName] = coinholdings[coinName].coin.ticker
				total += value
				portfolio.push({
					name: coinName,
					y: value
				})
			}
		}else{
			let name = "whole-lotta-nothin"
			let quantity = 1
			let lastTradePrice = 0
			let value = quantity * lastTradePrice
			quantities[name] = quantity
			coinNameToTicker[name] = "EMPTY PORTFOLIO"
			total += value
			portfolio.push({
				name: name,
				y: quantity
			})
		}
		return {
			"data": portfolio,
			"total": total,
			"quantities": quantities,
			"coinNameToTicker": coinNameToTicker
		}
	}

	highchartsOrError(){
		if (this.props.coinPrices.fetching === false && this.props.portfolio.fetching === false){
			return (
				<HighchartsReact
					highcharts={highcharts}
					options={chartOptions(this.getPortfolioProps())}
				/>
			)
		}else if (this.props.coinPrices.error || this.props.portfolio.error){
			return <b>Fetch to Server Failed</b>
		}else{
			return <div style={{height: 400}}></div>
		}
	}

	render(){
		return (
			<ChartContainer>
				{this.highchartsOrError()}
			</ChartContainer>
		)
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		loadPortfolio: PortfolioAPI.loadPortfolio,
		loadPortfolioPrices: CoinPricesAPI.loadMyCoinPrices
	}, dispatch)
}

function mapStateToProps(state){
	return {
		portfolio: state.portfolio,
		coinPrices: state.coinPrices,
		token: state.user.currentUser.token
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Portfolio)