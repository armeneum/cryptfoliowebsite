import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Switch, Route, withRouter } from 'react-router'
import { bindActionCreators } from 'redux'
import { push } from 'react-router-redux'

import styled from 'styled-components'

//My Components
import AddTransactions from './AddTransactions'
import ListTransactions from './ListTransactions'

//Material UI
	//Components
import Button from '@material-ui/core/Button'
	//Icons
import Add from '@material-ui/icons/Add';
import List from '@material-ui/icons/List';

const TransactionsContainer = styled.div`
`

const MyTabs = styled.div`
	padding-top: 15px;
`

const ButtonTab = styled.div`
	display: inline-block;
	width: 50%
`

const ButtonWrapper = styled.div`
	width: max-content;
	margin: auto;
`

const defaultTab = 'list'

class Transactions extends Component{

	constructor(props){
		super(props)

		this.state = {
			selectedTab: this.props.path.split('/')[3] || defaultTab,
			listButtonColor: "default",
			addButtonColor: "default"
		}

		this.changeTab = this.changeTab.bind(this)
	}

	componentDidMount(){
		if (['/dashboard/transactions','/dashboard/transactions/'].includes(this.props.path)){
			this.props.redirectTo('/dashboard/transactions/' + defaultTab + '/')
		}
		this.changeButtonColor(this.state.selectedTab)
	}

	changeTab(value){
		if(!this.props.path.includes(value)){
			this.setState({
				selectedTab: value
			})
			this.props.redirectTo('/dashboard/transactions/'+value+'/')
		}
		this.changeButtonColor(value)
	}

	changeButtonColor(button){
		if (button === "list"){
			this.setState({
				listButtonColor: "primary",
				addButtonColor: "default"
			})
		}else if (button === "add"){
			this.setState({	
				listButtonColor: "default",
				addButtonColor: "primary"
			})
		}else{
			console.log("changeButtonColor: INCORRECT ARGUMENT ERROR")
		}
	}

	render(){
		return	(
			<TransactionsContainer>
				<MyTabs>
					<ButtonTab>
						<ButtonWrapper>
							<Button 
								variant="fab"
								color={ this.state.listButtonColor }
								onClick={() => this.changeTab('list')}
							>
								<List/>
							</Button>
						</ButtonWrapper>
					</ButtonTab>
					<ButtonTab>
						<ButtonWrapper>
							<Button
								variant="fab"
								color={ this.state.addButtonColor }
								onClick={() => this.changeTab('add')}
							>
								<Add/>
							</Button>
						</ButtonWrapper>
					</ButtonTab>
				</MyTabs>
				<Switch>
					<Route path="/dashboard/transactions/list" component={ListTransactions}></Route>
					<Route path="/dashboard/transactions/add" component={AddTransactions}></Route>
				</Switch>
			</TransactionsContainer>
		)
	}
}

function mapStateToProps(state){
	return ({
		path: state.routing.location.pathname
	})
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		redirectTo: (path) => push(path)
	}, dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Transactions))