import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

//User-defined Components

//API Functions
import TransactionAPI from '../Transactions/TransactionAPI'

//Material UI
import EnhancedTable from './EnhancedTable'

//Styling
import styled from 'styled-components'
import 'react-select/dist/react-select.css'
import 'react-virtualized-select/styles.css'

let counter = 0;
function convertToTransaction(dataObj,dataType) {
	counter += 1;
	let transferType, buyCoin, buyQuantity, buyPrice, sellCoin, sellQuantity, sellPrice, exchange;
	if(dataType==='trade'){
		transferType = 'Trade'
		buyCoin = dataObj.buyCoin["ticker"]
		buyQuantity = dataObj.buyQuantity.replace(/0*$/,'').replace(/\.$/,'')
		buyPrice = dataObj.buyPrice["price"].replace(/0*$/,'').replace(/\.$/,'')
		sellCoin = dataObj.sellCoin["ticker"]
		sellQuantity = dataObj.sellQuantity.replace(/0*$/,'').replace(/\.$/,'')
		sellPrice = dataObj.sellPrice["price"].replace(/0*$/,'').replace(/\.$/,'')
		exchange = dataObj.exchange
	}else if(dataType==='transfer'){
		if (dataObj.exchange!=='N/A'){
			exchange = dataObj.exchange
		}
		if(['P','W'].includes(dataObj.transferType)){
			if (dataObj.transferType === "P"){
				transferType = "Purchase"
			}else if (dataObj.transferType === "W"){
				transferType = "Withdraw"
			}
			sellCoin = dataObj.coin["ticker"]
			sellQuantity = dataObj.quantity.replace(/0*$/,'').replace(/\.$/,'')
			sellPrice = dataObj.price["price"].replace(/0*$/,'').replace(/\.$/,'')
		}else if(['S','D'].includes(dataObj.transferType)){
			if (dataObj.transferType === "S"){
				transferType = "Sale"
			}else if (dataObj.transferType === "D"){
				transferType = "Deposit"
			}
			buyCoin = dataObj.coin["ticker"]
			buyQuantity = dataObj.quantity.replace(/0*$/,'').replace(/\.$/,'')
			buyPrice = dataObj.price["price"].replace(/0*$/,'').replace(/\.$/,'')
		}else{
			throw Error
		}
	}else{
		throw Error
	}
	return (
		{
			id: counter,
			date: dataObj.date,
			time: dataObj.time,
			transferType: transferType,
			buyCoin: buyCoin,
			buyQuantity: buyQuantity,
			buyPrice: buyPrice ? '$' + parseFloat(buyPrice).toFixed(2).replace(/\B(?=(\d{3})+\.)/g, ",") : undefined,
			sellCoin: sellCoin,
			sellQuantity: sellQuantity,
			sellPrice: sellPrice ? '$' + parseFloat(sellPrice).toFixed(2).replace(/\B(?=(\d{3})+\.)/g, ",") : undefined,
			fromAddress: dataObj.fromAddress,
			toAddress: dataObj.toAddress,
			exchange: exchange
		}
	)
}

const ListTransactionsContainer = styled.div`
	padding: 0px 15px 15px 15px;
`

class ListTransactions extends Component{

	componentWillMount(){
		this.props.fetchTransactions(this.props.token)
	}

	transactionTable(){
		const columnData = [
			{ id: 'date', numeric: false, disablePadding: true, label: 'Date' },
			{ id: 'time', numeric: false, disablePadding: true, label: 'Time' },
			{ id: 'transferType', numeric: false, disablePadding: true, label: 'Transfer Type' },
			{ id: 'buyCoin', numeric: false, disablePadding: true, label: 'Bought Coin' },
			{ id: 'buyQuantity', numeric: false, disablePadding: true, label: 'Bought Quantity' },
			{ id: 'buyPrice', numeric: false, disablePadding: true, label: 'Bought Coin Price' },
			{ id: 'sellCoin', numeric: false, disablePadding: true, label: 'Sold Coin' },
			{ id: 'sellQuantity', numeric: false, disablePadding: true, label: 'Sold Quantity' },
			{ id: 'sellPrice', numeric: false, disablePadding: true, label: 'Sold Coin Price' },
			{ id: 'fromAddress', numeric: false, disablePadding: true, label: 'From Address' },
			{ id: 'toAddress', numeric: false, disablePadding: true, label: 'To Address' },
			{ id: 'exchange', numeric: false, disablePadding: true, label: 'Exchange' }
		]

		let transactions = []
		let trades = this.props.transactions.trades
		let transfers = this.props.transactions.transfers

		if(this.props.transactions.fetching === false){
				for (let trade in trades){
					const tradeObj = trades[trade]
					transactions.push(convertToTransaction(tradeObj,'trade'))
				}
				for (let transfer in transfers){
					const transferObj = transfers[transfer]
					transactions.push(convertToTransaction(transferObj,'transfer'))
				}

			return 	(
				<EnhancedTable
					title="Transactions"
					columns={columnData}
					data={transactions}
				/>
			)
		}else{
			return (
				<div style={{height: 582}}>Loading</div>
			)
		}
	}

	render(){

		return (			
			<ListTransactionsContainer>
				{this.transactionTable()}
			</ListTransactionsContainer>
		)
	}
}

function mapDispatchToProps(dispatch){
	return (
		bindActionCreators({
			fetchTransactions: TransactionAPI.getTransactions
		}, dispatch)
	)
}

function mapStateToProps(state){
	return ({
		token: state.user.currentUser.token,
		transactions: state.transactions
	})
}

export default connect(mapStateToProps, mapDispatchToProps)(ListTransactions)