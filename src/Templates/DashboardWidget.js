import React, { Component } from 'react'
import { Switch, Route, withRouter } from 'react-router'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import styled from 'styled-components'

import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import PieChart from '@material-ui/icons/PieChart'
import StockChart from '@material-ui/icons/ShowChart'
import Exchange from '@material-ui/icons/ImportExport'

import Portfolio from './Portfolio'
import Performance from './Performance'
import Transactions from './Transactions'

const DashboardContainer = styled.div`
	width: 80%;
	min-width: 500px;
	margin: 50px auto;
	padding: 10px;
	border-radius: 20px;
	background-color: white;
`

const TabbedComponent = styled.div`
`

const Dashboard = styled.div`
	background-color: white;
`;

//eslint-disable-next-line
const Header = styled.h1`
	margin: 0;
	padding: 10px 0;
	font-size: 25px;
	text-align: center;
`;

class DashboardWidget extends Component {
	constructor(props){
		super(props)

		this.state = {
			selectedTab: this.props.path.split('/')[2] || "portfolio"
		}

		this.changeTab = this.changeTab.bind(this);
	}

	componentWillMount(){
		if (['/dashboard','/dashboard/'].includes(this.props.path)){
			this.props.redirectTo('/dashboard/portfolio')
		}
	}

	changeTab(event, value){
		if(!this.props.path.includes(value)){
			this.setState({
				selectedTab: value
			})
			this.props.redirectTo('/dashboard/'+value+'/')
		}
	}

	render(){
		return (
			<DashboardContainer>
				<Dashboard>
					<Tabs
						value={this.state.selectedTab}
						onChange={this.changeTab}
						centered
						fullWidth
						indicatorColor="primary"
						textColor="primary"
					>
						<Tab value="portfolio" icon={<PieChart />} label="PORTFOLIO" />
						<Tab value="performance" icon={<StockChart />} label="PERFORMANCE" />
						<Tab value="transactions" icon={<Exchange />} label="TRANSACTIONS" />
					</Tabs>
					<TabbedComponent>
						<Switch>
							<Route path="/dashboard/portfolio/" component={Portfolio}></Route>
							<Route path="/dashboard/performance/" component={Performance}></Route>
							<Route path="/dashboard/transactions/" component={Transactions}></Route>
						</Switch>
					</TabbedComponent>
				</Dashboard>
			</DashboardContainer>
		);
	}
}

function mapStateToProps(state){
	return {
		path: state.routing.location.pathname
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({
		redirectTo: (suffix) => push(suffix)
	}, dispatch)
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(DashboardWidget))