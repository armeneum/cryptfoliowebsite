import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import configureStore from './store/configureStore';
import { Switch, Route } from 'react-router'
import { ConnectedRouter, routerMiddleware } from 'react-router-redux'
import Login from './Templates/Login';
import Signup from './Templates/Signup';
import Dashboard from './Templates/Dashboard';
import IndexRedirector from './Templates/IndexRedirector';
import registerServiceWorker from './Templates/registerServiceWorker';
import createHistory from 'history/createBrowserHistory'

const history = createHistory()
const middlewareFromRouter = routerMiddleware(history)
const store = configureStore(middlewareFromRouter);

ReactDOM.render(
  <Provider store={store}>
	<ConnectedRouter history={history}>
		<div>
			<Switch>
				<Route exact path="/login" component={Login}></Route>
				<Route exact path="/signup" component={Signup}></Route>
				<Route path="/dashboard" component={Dashboard}></Route>
				<Route component={IndexRedirector}></Route>
			</Switch>
		</div>
	</ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();