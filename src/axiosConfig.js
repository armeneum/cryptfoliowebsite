export default (token=undefined) => {
	if(token===undefined){
		return {
			baseURL: process.env.REACT_APP_BACKEND_BASE_URL,
			headers: {'Content-Type': 'application/json'}
		}
	}else{
		const fullToken = 'Token ' + token
		return {
			baseURL: process.env.REACT_APP_BACKEND_BASE_URL,
			headers: {
				'Content-Type': 'application/json',
				'Authorization': fullToken
			}
		}
	}
}